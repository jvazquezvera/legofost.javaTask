# LegoJavaService 

La finalidad de este proyecto es tener una Custom task para jbpm 6.4, la cual llame a un metodo de una clase java y le pase como parametro el KContext para poder agreagr y leer variables del proceso, esta compuesto por:

 * La clase: com.mx.legosoft.custom.services.LegoJavaService que implementa la interfaz org.kie.api.runtime.process.WorkItemHandler 
 * El archivo legosoft_custom.wid que sirve como descriptor para que la custom task este disponible en la paleta
 

Uso:

Una vez compilado este proyecto, si se va usar en algun proyecto lo tenemos que agregar como dependencia:

```maven
    <dependency>
       <groupId>com.mx.legosoft.jbpm.handlers</groupId>
       <artifactId>legosofthandlers</artifactId>
       <version>1.0.0-SNAPSHOT</version>
    </dependency>
   ```

En el proyecto donde vamos a usar nuestra custom task creamos el archivo legosoft_custom.wid dentro de src/main/resources/META-INF con el siguiente contenido:

```maven
	
import org.drools.core.process.core.datatype.impl.type.StringDataType;
import org.drools.core.process.core.datatype.impl.type.ObjectDataType;

[  
  [
    "name" : "LegoJavaService",
    "parameters" : [
        "class" : new StringDataType(),
        "method" : new StringDataType(),
    ],
    "results" : [
        "Result" : new ObjectDataType(),
    ],
    "displayName" : "LegoJavaService",
    "icon" : "defaultservicenodeicon.png",
    "defaultHandler" : "mvel: new com.mx.legosoft.custom.services.LegoJavaService()",
  ]
]


```

Agregramos una entrada al archivo src/main/resources/META-INF/kie-deployment-descriptor.xml en la seccion work-item-handlers

```
  <work-item-handler>
      <resolver>mvel</resolver>
      <identifier>new com.mx.legosoft.custom.services.LegoJavaService(ksession)</identifier>
      <parameters/>
      <name>Legosoftcustomtask</name>
   </work-item-handler>
```

Para usarlo dentro del entorno de eclipse se tiene agregar de la siguiente manera:

```
	ksession.getWorkItemManager().registerWorkItemHandler("LegoJavaService", new
				LegoJavaService(ksession));
```


En nuestro diagrama solo tenemos te arrastrarlo y colocar en las propiedades 

* class: el nombre calicado de la clase java   "com.mx.paquete.clase"
* method: nombre del metodo a invocar este debe tener como parametro un objeto del tipo: org.drools.core.spi.ProcessContext

Ejemplo:

```java
import org.drools.core.spi.ProcessContext;

public class ClasePruebaTaskService {
	
	public void metodo2(ProcessContext ctx){
		String gg= (String) ctx.getVariable("out");
		System.out.println("obteniedo el valor: "+ctx.getVariable("out"));
	}
	
}
```

