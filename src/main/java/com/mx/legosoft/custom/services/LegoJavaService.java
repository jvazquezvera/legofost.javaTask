package com.mx.legosoft.custom.services;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.kie.api.runtime.KieSession;
import org.drools.core.spi.ProcessContext;
import org.jbpm.bpmn2.handler.WorkItemHandlerRuntimeException;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.kie.api.runtime.process.WorkflowProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author jvazquez
 *
 */
public class LegoJavaService implements WorkItemHandler {

	private KieSession ksession;
	private static final Logger logger = LoggerFactory.getLogger(LegoJavaService.class);
    
	
	public LegoJavaService(KieSession ksession) {
		super();
		this.ksession = ksession;
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		logger.debug("Manejando task legosfot java");
		ProcessContext kcontext = new ProcessContext(ksession);
		WorkflowProcessInstance processInstance = (WorkflowProcessInstance)      
				  ksession.getProcessInstance(workItem.getProcessInstanceId());
	    kcontext.setProcessInstance(processInstance);  
	   
	    
     	String clazz = (String) workItem.getParameter("class");
	    String method = (String) workItem.getParameter("method"); 

	    
	    if (clazz == null || clazz.isEmpty() ) {
	    	 throw new IllegalArgumentException("Class is a required parameter");
	    }
	    
	    if (method == null || method.isEmpty()) {
	    	  throw new IllegalArgumentException("Method is a required parameter");
        }
	    
	    
	    Class<?> c = null;
	    
	    try {
            c = Class.forName(clazz);
        } catch (ClassNotFoundException cnfe) {
        	cnfe.printStackTrace();
        	handleException(cnfe, clazz, method);
        }
	    
	    try {
	    	
            Object instance = c.newInstance();
            Method metodo = c.getMethod(method, ProcessContext.class);
            Object result = metodo.invoke(instance, kcontext);
            Map<String, Object> results = new HashMap<String, Object>();
            results.put("result", result);
            manager.completeWorkItem(workItem.getId(), results);
            
	    }catch (InstantiationException ie) {
	    	handleException(ie, clazz, method);
        } catch (IllegalAccessException iae) {
        	handleException(iae, clazz, method);
        } catch (NoSuchMethodException nsme) {
        	handleException(nsme, clazz, method);
        } catch (InvocationTargetException ite) {
        	handleException(ite, clazz, method);
        } catch( Throwable cause ) { 
        	handleException(cause, clazz, method);
        }
	        
	}

	
	
	private void handleException(Throwable cause, String clazz, String method) { 
        logger.debug("Handling exception {} inside service logosfot java con la clase  {} y el metodo value {}",
                cause.getMessage(), clazz, method);
        WorkItemHandlerRuntimeException wihRe;
        if( cause instanceof InvocationTargetException ) { 
            Throwable realCause = cause.getCause();
            wihRe = new WorkItemHandlerRuntimeException(realCause);
            wihRe.setStackTrace(realCause.getStackTrace());
        } else { 
            wihRe = new WorkItemHandlerRuntimeException(cause);
            wihRe.setStackTrace(cause.getStackTrace());
        }
        wihRe.setInformation("Clase", clazz);
        wihRe.setInformation("method", method);
        wihRe.setInformation(WorkItemHandlerRuntimeException.WORKITEMHANDLERTYPE, this.getClass().getSimpleName());
        throw wihRe;
        
    }
	

	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		// TODO Auto-generated method stub
		
	}


}
